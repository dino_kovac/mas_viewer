package hr.fer.mas.projekt;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Dino on 12/22/13.
 */
public class ConnectThread extends Thread {

    private ScheduledExecutorService scheduler;
    private boolean readerConnected = false;
    private int PERIOD_SECONDS = 1;

    public ConnectThread() {
        scheduler = Executors.newSingleThreadScheduledExecutor();
    }

    @Override
    public void run() {
        scheduler.scheduleAtFixedRate(connectTask, 0, PERIOD_SECONDS, TimeUnit.SECONDS);
    }

    public void cancel() {
        if (!scheduler.isShutdown()) {
            scheduler.shutdown();
        }
    }

    private Runnable connectTask = new Runnable() {
        @Override
        public void run() {

            boolean oldState = readerConnected;

            if (CardReaderHelper.getCardReader() != null) {
                if (!readerConnected) {
                    if (CardReaderHelper.connectToReader() >= 0) {
                        readerConnected = true;
                    } else {
                        readerConnected = false;
                    }
                }
            } else {
                readerConnected = false;
            }

            if (readerConnected != oldState) {
                Debug.log("Card reader connected: " + readerConnected);
                MessageBus.publish(new ConnectionChange(readerConnected));
            }
        }
    };

    public static class ConnectionChange {
        private boolean isConnected = false;

        public ConnectionChange(boolean isConnected) {
            this.isConnected = isConnected;
        }

        public boolean isConnected() {
            return isConnected;
        }
    }
}
