package hr.fer.mas.projekt;

import net.engio.mbassy.listener.Handler;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.util.Calendar;

/**
 * Created by Dino on 12/17/13.
 */
public class MASViewer extends JFrame {

    // ACS VendorId 0x072f
    // ProductId 0x8003

    private final JLabel statusLabel = new JLabel("Dohvaćam sliku..");
    private static ConnectThread connectThread;
    private byte[] key;
    private CardReaderHelper.ReadKeyThread readKeyThread;
    private Border paddingBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);
    private JTextArea debugLogTextArea;
    private JScrollPane debugLogScrollPane;
    private JLabel imageLabel;
    private JButton retryButton;
    private static MASViewer masViewer;

    public MASViewer() {

        MessageBus.subscribe(this);

        setTitle("MAS Viewer");
        addWindowListener(windowListener);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        // We create a panel which will hold the UI components
        JPanel pane = new JPanel(new BorderLayout());

        pane.setLayout(new BoxLayout(pane, BoxLayout.PAGE_AXIS));

        pane.add(statusLabel);
        retryButton = new JButton("Pokušaj ponovno");
        retryButton.setEnabled(false);
        retryButton.setMargin(new Insets(10, 10, 10, 10));
        pane.add(retryButton);
        imageLabel = new JLabel();
        imageLabel.setBorder(paddingBorder);
        imageLabel.setHorizontalAlignment(SwingConstants.CENTER);
        pane.add(imageLabel);
        debugLogTextArea = new JTextArea(5, 30);
        debugLogTextArea.setBorder(paddingBorder);
        debugLogTextArea.setEditable(false);
        debugLogTextArea.setBackground(new Color(0x2b, 0x2b, 0x2b));
        debugLogTextArea.setForeground(new Color(0x49, 0x6b, 0x43));
        ((DefaultCaret) debugLogTextArea.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
        debugLogScrollPane = new JScrollPane(debugLogTextArea);
        setPreferredSize(new Dimension(500, 600));
        JLabel logLabel = new JLabel("Debug log:");
        logLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
        pane.add(logLabel);
        pane.add(debugLogScrollPane);
        pane.setBorder(paddingBorder);

        retryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                retryButton.setEnabled(false);
                statusLabel.setText("Spojite čitač.");
                if (connectThread != null) {
                    connectThread.cancel();
                }
                connectThread = new ConnectThread();
                connectThread.start();
            }
        });

        //onEvent(new ConnectThread.ConnectionChange(false));

        // add the pane to the main window
        getContentPane().add(pane);

        // Pack will make the size of window fitting to the components
        // You could also use for example setSize(300, 400);
        pack();

    }

    public void start() {
        try {
            Debug.log("Fetching image..");
            Process p = Runtime.getRuntime().exec("python serija.py");
            p.waitFor();
            Debug.log("Image fetching finished.");
            statusLabel.setText("Spojite čitač.");
            connectThread = new ConnectThread();
            connectThread.start();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            Debug.log("Image fetch failed:\n" + e.getMessage());
        }
    }

    private WindowListener windowListener = new WindowListener() {
        @Override
        public void windowOpened(WindowEvent e) {

        }

        @Override
        public void windowClosing(WindowEvent e) {
            if (connectThread != null) {
                connectThread.cancel();
            }
            if (readKeyThread != null) {
                readKeyThread.cancel();
            }
            MessageBus.unsubscribe(MASViewer.this);
        }

        @Override
        public void windowClosed(WindowEvent e) {

        }

        @Override
        public void windowIconified(WindowEvent e) {

        }

        @Override
        public void windowDeiconified(WindowEvent e) {

        }

        @Override
        public void windowActivated(WindowEvent e) {

        }

        @Override
        public void windowDeactivated(WindowEvent e) {

        }
    };

    @Handler
    public void onEvent(ConnectThread.ConnectionChange event) {
        if (event.isConnected()) {
            statusLabel.setText("Čekam autorizaciju...");
            if (readKeyThread != null) {
                readKeyThread.cancel();
            }
            readKeyThread = new CardReaderHelper.ReadKeyThread();
            readKeyThread.start();
        } else {
            statusLabel.setText("Čitač nije spojen!");
            if (readKeyThread != null) {
                readKeyThread.cancel();
            }
        }
    }

    @Handler
    public void onEvent(CardReaderHelper.KeyReadEvent event) {
        key = event.getKey();
        Calendar now = Calendar.getInstance();
        String filename = "decrypted_" + now.getTimeInMillis() + ".jpg";
        try {
            DecryptionHelper.fileXOR("image_encrypted.jpg", key, filename);
        } catch (IOException e) {
            e.printStackTrace();
            Debug.log("Decryption failed: " + e.getMessage());
        }
        statusLabel.setText("Ključ uspješno pročitan.");
        imageLabel.setIcon(new ImageIcon(filename));
    }

    @Handler
    public void onEvent(CardReaderHelper.ReadingKeyFailedEvent event) {
        retryButton.setEnabled(true);
    }

    @Handler
    public void onEvent(String logMessage) {
        debugLogTextArea.setText(debugLogTextArea.getText() + logMessage + "\n");
    }

    public static void main(String[] args) {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        masViewer = new MASViewer();
        masViewer.setVisible(true);
        masViewer.start();
    }
}
