package hr.fer.mas.projekt;

import net.engio.mbassy.bus.MBassador;
import net.engio.mbassy.bus.config.BusConfiguration;

/**
 * Created by Dino on 12/22/13.
 */
public class MessageBus {

    private static MBassador<Object> bus;

    private static void init() {
        bus = new MBassador<>(BusConfiguration.Default());
    }

    public static void subscribe(Object o) {
        if (bus == null) {
            init();
        }

        bus.subscribe(o);
    }

    public static void unsubscribe(Object o) {
        if (bus != null) {
            bus.unsubscribe(o);
        }
    }

    public static void publish(Object o) {
        if (bus == null) {
            init();
        }

        bus.publish(o);
    }

}
