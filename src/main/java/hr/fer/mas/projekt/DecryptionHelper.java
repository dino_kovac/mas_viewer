package hr.fer.mas.projekt;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by Dino on 1/20/14.
 */
public class DecryptionHelper {

    public static void fileXOR(String inputFile, byte[] key, String outputFile) throws IOException {

        byte[] data = Files.readAllBytes(Paths.get(inputFile));
        FileOutputStream out = new FileOutputStream(outputFile);

        int counter = 0;

        for (byte read : data) {
            out.write(read ^ key[counter % (key.length - 1)]);
            counter++;
        }

        out.flush();
        out.close();
    }

}
