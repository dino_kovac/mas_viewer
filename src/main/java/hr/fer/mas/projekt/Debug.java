package hr.fer.mas.projekt;

/**
 * Created by Dino on 12/22/13.
 */
public class Debug {

    public static void log(String message) {
        System.out.println(message);
        MessageBus.publish(message);
    }

}
