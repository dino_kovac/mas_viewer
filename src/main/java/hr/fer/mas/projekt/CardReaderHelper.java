package hr.fer.mas.projekt;

import acs.jni.ACR120U;

import javax.usb.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dino on 12/17/13.
 */
public class CardReaderHelper {

    public static final short ACSVendorId = 0x072f;
    final protected static char[] hexArray = "0123456789abcdef".toCharArray();
    public static ACR120U acr120u = new ACR120U();
    private static short readerHandle = -1;
    private static short sector = 1;
    private static short block = 6;
    private static short keyAddress = 1;
    private static byte[] zeros = new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00};
    private static byte[] ffs = new byte[]{(byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff};
    private static byte[] factoryKey = new byte[]{(byte) 0xa0, (byte) 0xa1, (byte) 0xa2, (byte) 0xa3, (byte) 0xa4, (byte) 0xa5};
    private static byte[] coolKey = new byte[]{(byte) 0xde, (byte) 0xad, (byte) 0xbe, (byte) 0xef, (byte) 0xba, (byte) 0xad, (byte) 0xc0, (byte) 0xde, (byte) 0xde, (byte) 0xad, (byte) 0xbe, (byte) 0xef, (byte) 0xba, (byte) 0xad, (byte) 0xc0, (byte) 0xde};

    private static UsbDevice getDeviceByVendorId(final UsbDevice device, short vendorId) {

        if (device.getUsbDeviceDescriptor().idVendor() == vendorId) {
            return device;
        }

        // Dump child devices if device is a hub
        if (device.isUsbHub()) {
            final UsbHub hub = (UsbHub) device;
            for (UsbDevice child : (List<UsbDevice>) hub.getAttachedUsbDevices()) {
                UsbDevice d = getDeviceByVendorId(child, vendorId);
                if (d != null) {
                    return d;
                }
            }
        }

        return null;
    }

    public static UsbDevice getCardReader() {

        final UsbServices services;

        try {
            services = UsbHostManager.getUsbServices();
            return getDeviceByVendorId(services.getRootUsbHub(), ACSVendorId);

        } catch (UsbException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static short connectToReader() {

        List<Short> ports = new ArrayList<>(8);
        ports.add(ACR120U.ACR120_USB1);
        ports.add(ACR120U.ACR120_USB2);
        ports.add(ACR120U.ACR120_USB3);
        ports.add(ACR120U.ACR120_USB4);
        ports.add(ACR120U.ACR120_USB5);
        ports.add(ACR120U.ACR120_USB6);
        ports.add(ACR120U.ACR120_USB7);
        ports.add(ACR120U.ACR120_USB8);

        short retval;

        // try connecting to the reader, we don't know which port to use so we need to try them all
        for (Short port : ports) {
            retval = acr120u.open(port);
            if (retval >= 0) {
                readerHandle = retval;
                return retval;
            }
        }

        return -1;
    }

    public static class ReadKeyThread extends Thread {

        private boolean cancelled = false;

        public void cancel() {
            cancelled = true;
        }

        @Override
        public void run() {

            byte[] pNumTagFound = new byte[1];
            byte[] pTagType = new byte[ACR120U.ACR120UJNI_MAX_NUM_TAGS];
            byte[] pTagLength = new byte[ACR120U.ACR120UJNI_MAX_NUM_TAGS];
            byte[][] pSN = new byte[ACR120U.ACR120UJNI_MAX_NUM_TAGS][10];
            pNumTagFound[0] = 0;
            short status = -1;

            Debug.log("Searching for card...");

            while (pNumTagFound[0] <= 0) {
                if (cancelled) {
                    return;
                }
                status = acr120u.listTags(readerHandle, pNumTagFound, pTagType, pTagLength, pSN);
                Debug.log(".");
//                if (status < 0) {
//                    return;
//                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {

                }
            }

            Debug.log("Found card! Selecting it now.");

            byte[] pResultTagType = new byte[1]; // Mifare 1K 0x02
            byte[] pResultTagLength = new byte[1]; // Mifare 1K 0x04
            byte[] pResultSN = new byte[10];

            status = acr120u.select(readerHandle, pResultTagType, pResultTagLength, pResultSN);

            if (status != 0) {
                Debug.log("Error selecting card: " + status + "! Aborting.");
                MessageBus.publish(new ReadingKeyFailedEvent());
                return;
            }

            if (cancelled) {
                return;
            }

            int type = pResultTagType[0];
            int length = pResultTagLength[0];

            Debug.log("Status: " + status + " Type: " + Integer.toHexString(type) + " Length: " + Integer.toHexString(length) + " Serial: " + bytesToHex(pResultSN));
            byte[] empty = new byte[6];
            status = acr120u.login(readerHandle, (byte) sector, (byte) ACR120U.AC_MIFARE_LOGIN_KEYTYPE_A, (byte) keyAddress, ffs);
            if (status != 0) {
                Debug.log("Error with login: " + status + "! Aborting.");
                MessageBus.publish(new ReadingKeyFailedEvent());
                return;
            }

            // Assume the Sector is authorized, then “Read data from Block
            // 0x08 of Sector 0x02”
            byte[] data = new byte[16];
            status = acr120u.read(readerHandle, toBytes(block)[0], data);
//            status = acr120u.write(readerHandle, toBytes(block)[0], coolKey);
            if (status != 0) {
                Debug.log("Error while reading data: " + status + "! Aborting.");
                MessageBus.publish(new ReadingKeyFailedEvent());
                return;
            }

            String keyFromCard = bytesToHex(data);
            Debug.log("Read key from card: " + keyFromCard);

            MessageBus.publish(new KeyReadEvent(data));
        }
    }

    public static class KeyReadEvent {

        private byte[] key;

        public KeyReadEvent(byte[] key) {

            this.key = key;
        }

        public byte[] getKey() {
            return key;
        }
    }

    public static class ReadingKeyFailedEvent {

    }

    public static byte[] toBytes(short s) {
        return new byte[]{(byte) (s & 0x00FF), (byte) ((s & 0xFF00) >> 8)};
    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}